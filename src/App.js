import React, { Component } from 'react';
import './App.scss';
import ATM from './ATM.js';

class App extends Component {
  render() {
    return (
      <ATM />
    );
  }
}

export default App;
