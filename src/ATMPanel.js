import React, { Component } from 'react';

class ATMPanel extends Component {
  render() {
    return (
      <div className="atm-panel">
        <div className="atm-left-buttons">
          <div className="atm-button" onClick={this.props.actions[0].action}></div>
          <div className="atm-button" onClick={this.props.actions[1].action}></div>
          <div className="atm-button" onClick={this.props.actions[2].action}></div>
          <div className="atm-button" onClick={this.props.actions[3].action}></div>
        </div>
        <div className="atm-left-outset-bars">
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
        </div>
        <div className="atm-screen">
          <div className="atm-screen-info">
            <p>{ this.props.display }</p>
            <p>{ this.props.entry }</p>
          </div>
          <div className="atm-screen-options">
            <div className="atm-inset-bars">
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
            </div>
            <div className="atm-screen-options-left">
              <div className="atm-screen-option">{this.props.actions[0].value}</div>
              <div className="atm-screen-option">{this.props.actions[1].value}</div>
              <div className="atm-screen-option">{this.props.actions[2].value}</div>
              <div className="atm-screen-option">{this.props.actions[3].value}</div>
            </div>
            <div className="atm-screen-options-right">
              <div className="atm-screen-option">{this.props.actions[4].value}</div>
              <div className="atm-screen-option">{this.props.actions[5].value}</div>
              <div className="atm-screen-option">{this.props.actions[6].value}</div>
              <div className="atm-screen-option">{this.props.actions[7].value}</div>
            </div>
            <div className="atm-inset-bars">
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
              <div className="atm-inset-bar"></div>
            </div>
          </div>
        </div>
        <div className="atm-right-outset-bars">
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
          <div className="atm-outset-bar"></div>
        </div>
        <div className="atm-right-buttons">
          <div className="atm-button" onClick={this.props.actions[4].action}></div>
          <div className="atm-button" onClick={this.props.actions[5].action}></div>
          <div className="atm-button" onClick={this.props.actions[6].action}></div>
          <div className="atm-button" onClick={this.props.actions[7].action}></div>
        </div>
      </div>
    );
  }
}

export default ATMPanel;
