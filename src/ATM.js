import React, { Component } from 'react';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import ATMPanel from './ATMPanel.js';
import './ATM.scss';

const defaultAction = () => { console.log('clicked action button'); };

class ATM extends Component {
  constructor(props) {
    super(props);

    const actions = [
      { action: defaultAction, value: null },
      { action: defaultAction, value: null },
      {
        action: () => {
          if (this.isCardValid()) {
            fetch('/withdraw').then(
              res => console.log('withdrawing... ', res)
            );
          }
        },
        value: 'Withdraw'
      },
      {
        action: () => {
          if (this.isCardValid()) {
            fetch('/deposit').then(
              res => console.log('depositing funds... ', res)
            );
          }
        },
        value: 'Deposit'
      },
      { action: defaultAction, value: null },
      { action: defaultAction, value: null },
      {
        action: () => {
          if (this.isCardValid()) {
            fetch('/balance').then(
              res => console.log('checking balance... ', res)
            );
          }
        },
        value: 'Balance'
      },
      {
        action: () => {
          this.setState({ entry: '' });
        },
        value: 'Re-Enter PIN'
      }
    ];

    this.enterNumber = (key) => {
      // backspace removes the last number entered
      if (key === 'backspace') {
        this.setState({
          entry: this.state.entry.slice(0, this.state.entry.length-1)
        });
        return;
      }

      // do not allow more than 4 digits
      if (this.state.entry.length > 3) return;

      this.setState({ entry: this.state.entry + key });
    }

    this.state = {
      actions: actions,
      display: 'Enter PIN code...',
      entry: ''
    };
  }

  isCardValid() {
    return this.state.entry.length === 4;
  }

  cardClassName() {
    // 4-digit codes only
    if (this.state.entry.length === 4) {
      return 'active-' + this.state.entry.slice(0,1);
    }
    return '';
  }

  render() {
    return (
      <div className="atm-container">
        <KeyboardEventHandler
          handleKeys={['numeric', 'backspace']}
          onKeyEvent={(key, e) => { this.enterNumber(key); }} />
        <div className="atm-heading">
          <div className="atm-sign">
            <div className="atm-graffiti"></div>
          </div>
        </div>
        <div className="atm-body">
          <div className="atm-cards">
            <div className={ "atm-cards-mask " + this.cardClassName() }>
              <div className="pre-mask"></div>
              <div className="mask"></div>
              <div className="post-mask"></div>
            </div>
          </div>
          <ATMPanel
            actions={this.state.actions}
            display={this.state.display}
            entry={this.state.entry} />
          <div className="atm-trademark"></div>
          <div className="atm-graffiti-bottom"></div>
        </div>
      </div>
    );
  }
}

export default ATM;
