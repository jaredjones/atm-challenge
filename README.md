This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installing Dependencies
Run `yarn` or `npm` to install dependencies.

## Run
To run, use `yarn run start` or `npm run start`.

## Use
Type a 4-digit PIN code. Backspace removes last entry. Credit card symbols at the top will light up based on the first number of the PIN: 1xxx for STAR, 2xxx for pulse, 3xxx for Maestro, etc.

Withdraw/Deposit/Balance buttons will make an HTTP call if the PIN code is valid (4-digits). The result of these calls are output on the console.

Re-Enter PIN button will reset the PIN code entered.